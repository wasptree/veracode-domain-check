package main

import (
	"bufio"
	"crypto/tls"
	"fmt"
	"net"
	"net/http"
	"os"
	"time"
)

func main() {
	// Open the file that contains the list of domains to test
	file, err := os.Open("domains.txt")
	if err != nil {
		fmt.Println("Error opening file:", err)
		return
	}
	defer file.Close()
	fmt.Printf(" --- Veracode Domain Connectivity Test --- \n\n")
	// Read the domains from the file and test HTTPS connectivity to each one
	var domain string
	scanner := bufio.NewScanner(file)
	fmt.Printf(" [+] Testing HTTPS connectivity:\n")
	for scanner.Scan() {
		domain = scanner.Text()

		// Test HTTPS connectivity with certificate validation enabled
		_, err := http.Get("https://" + domain)
		if err != nil {
			fmt.Printf("    [-] %s - FAILED: \n        [!] %v\n", domain, err)
			fmt.Printf("    [!] Testing %s with SSL/TLS certificate validation disabled:\n", domain)
		        // Test HTTPS connectivity with certificate validation disabled
	        tr := &http.Transport{
				TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
				DialContext: (&net.Dialer{
					Timeout:   10 * time.Second,
					KeepAlive: 30 * time.Second,
				}).DialContext,
				IdleConnTimeout:       30 * time.Second,
				TLSHandshakeTimeout:   10 * time.Second,
				ExpectContinueTimeout: 1 * time.Second,
	        }
			client := &http.Client{Transport: tr}
			_, err = client.Get("https://" + domain)
			if err != nil {
				fmt.Printf("        [-] %s - FAILED: \n           [!] %v\n", domain, err)
			} else {
				fmt.Printf("        [+] %s - SUCCESS \n", domain)
			}
		} else {
		fmt.Printf("    [+] %s - SUCCESS \n", domain)
	}
	if err := scanner.Err(); err != nil {
		fmt.Println("Error reading file:", err)
		}
	}
}

