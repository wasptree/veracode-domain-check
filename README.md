# Veracode Domain Check
<!-- ABOUT THE PROJECT -->
## About This Project

veracode-domain-check is a Go script to test connectivity to Veracode domains and API endpoints. 

It can be run in a CI pipeline or on a workstation to test connectivity and idenitify potential firewall issues.


<!-- GETTING STARTED -->
## Getting Started

Pre built package for Windows AMD64 and Linux AMD64 is available under releases. Alterntively clone the project and adjust the build 

Simply download the package and run the script relevant to the OS. 
The domains tested are stored in domains.txt 

Linux
  ```
    wget https://gitlab.com/wasptree/veracode-domain-check/-/archive/latest/veracode-domain-check-latest.zip
    unzip veracode-domain-check-latest.zip && cd veracode-domain-check
    ./veracode-domain-check-linux-amd64
  ```

Mac OSX ( Intel )
  ```
    curl https://gitlab.com/wasptree/veracode-domain-check/-/archive/latest/veracode-domain-check-latest.zip -O
    unzip veracode-domain-check-latest.zip && cd veracode-domain-check-latest
    ./veracode-domain-check-darwin-amd64
  ```

Windows Powershell
  ```
    $URL = "https://gitlab.com/wasptree/veracode-domain-check/-/archive/latest/veracode-domain-check-latest.zip"
    $Path="./veracode-domain-check-latest.zip"
    Invoke-WebRequest -URI $URL -OutFile $Path
    Expand-Archive .\veracode-domain-check-latest.zip -DestinationPath .
    cd veracode-domain-check-latest
    ./veracode-domain-check-windows-amd64.exe
  ```

<!-- Pipeline USAGE EXAMPLES -->
## gitlab-ci Example

The script can be called from the pipeline to ensure your CI host has connectivity to the Veracode platform for upload and scan.

```
check_veracode_connectivity:
    image: wasptree/api-wrapper-java
    stage: connectivity_test
    script:
     - wget https://gitlab.com/wasptree/veracode-domain-check/-/archive/latest/veracode-domain-check-latest.zip
     - unzip veracode-domain-check-latest.zip && cd veracode-domain-check-latest
     - ./veracode-domain-check-linux-amd64
```

<!-- EXAMPLE Output -->
## Example Output

![](images/2023-02-24-16-18-18.png)